<?php
// Text
$_['text_home']          = 'Principal';
$_['text_wishlist']      = 'Lista de Desejos '; //(%s)
$_['text_shopping_cart'] = 'Carrinho ';
$_['text_category']      = 'Departamentos';
$_['text_account']       = 'Minha Conta';
$_['text_register']      = 'Cadastre-se';
$_['text_login']         = 'Acessar';
$_['text_order']         = 'Histórico de pedidos';
$_['text_transaction']   = 'Transações';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Sair';
$_['text_checkout']      = 'Finalizar Pedido';
$_['text_search']        = 'Busca';
$_['text_all']           = 'Exibir';
$_['text_category']      = 'CILINDROS';
//['$text_sale'] 		 = 'Promoção';
