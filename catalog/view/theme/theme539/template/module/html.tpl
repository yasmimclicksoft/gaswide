<?php if($heading_title == 'box_cinza_fr'){ ?>

<div class="box_html <?php echo $heading_title; ?>">

<div class="container"><h1 id="dif_h1">NOSSOS DIFERENCIAIS</h1>
	<div class="row dif_row">
	<div class="col-sm-4 box_dif">
		<div class="float-box">
			<div class="box_aside">
				<img class="icone_df" src="image/catalog/entrega.png"/> 
				<span class="lbl_dif">Entrega</span>
			</div>
				<div class="box_cnt"><h3></h3>
			</div>
			 <p>
			   Os melhores prazos de entrega do mercado.<br>
			   Para as unidades em estoque, o material é <br>
		       embarcado e no máximo até 48 horas após<br>
	           o fechamento do pedido.				 
			</p>
		</div>
	</div>
	<div class="col-sm-4 box_dif">
		<div class="float-box">
			<div class="box_aside">
				<img class="sifrao" src="image/catalog/preco.png"/> 
				<span class="lbl_dif">Preço</span>
			</div>
			<div class="box_cnt">
				<h3></h3>
			</div>

		     <p>
		    	Somos a única loja online especializada em<br>
				cilindros para gases do mercado nacional.<br>
				Grandes contratos de compra com nossos<br>
				fabricantes nos permitem oferecer o melhor<br>
				preço aos nossos clientes. Compare!
			 </p>
		</div>
	</div>
	<div class="col-sm-4 box_dif">
		<div class="float-box">
		<div class="box_aside">
		 <img class="icone_df" src="image/catalog/atendimento.png"/> 
		 <span class="lbl_dif">Atendimento</span>
		</div>
		<div class="box_cnt">
			<h3></h3>
		</div>
		   <p>
			  Uma equipe comercial técnica e dinâmica a<br>
	          sua disposição, sempre apta a indicar o<br>
			  produto certo para sua necessidade, dentro<br>
	 		  das melhores condições de mercado.
			</p>
		</div>
	</div>
	</div>
</div>
</div>

<?php } elseif ($heading_title == 'box_verde_fr') { ?>

	  <div id="estatico" class="box_html <?php echo $heading_title; ?>">								
		<div id="box-gradiente" class="row">
			<div class="col-sm-2">
			 <br>
			 <img id="cil" src="image/catalog/cilindro.png"/>
			</div>
			<div class="col-sm-3">
			  <p id="p1">
			   A PRIMEIRA</br>LOJA ONLINE</br> ESPECIALIZADA</br> EM CILINDROS</br> PARA GASES</br> DO AR
			  </p>
		   </div>

		   <div id="wrap" class="col-sm-6">
			  <div class="paragraf">
			  <p id="p2">
					 Seja bem-vindo! Aqui você vai encontrar uma enorme variedade</br>
					 em cilindros para gases para aplicações comuns do seu dia a dia,</br>
			  </p>
			  <p id="p_2">	
					 de forma simples, prática e dinâmica.
			  </p>
			  <p id="p2">Os produtos  dos  maiores  e  mais  renomados fabricantes do<br>
						 mundo reunidos em um só lugar, sempre com grandes estoques<br>
						 para pronta entrega, os melhores preços do mercado e a<br>
						 segurança do atendimento às normas de todas as autoridades<br>
			   </p> 
			   <p id="p_2">
					 regulatórias brasileiras e internacionais do setor.
			   </p>	 
				 <br>
			  <p id="p3" class="frase_bold">PODE COMPARAR, NÃO HÁ MELHOR PREÇO NA INTERNET!</p>
			  </div>
										
			  <div id="unidades" class="col-sm-12">
			    <img id="seta" src="image/catalog/seta1.png" type="button" data-toggle="dropdown" class="dropdown-toggle"/>
				<span id="deseja">DESEJA MAIS DE <b>9 UNIDADES?</b></span>			
		  		<p id="texto" class="dropdown-menu col-sm-12">			
				  Preços muito diferenciados para quantidades acima de 09 unidades.
				  <br>
				  Consulte-nos para compras no atacado! Ligue (21) 2260 8724.
				</p>
				<script>
			    	$(document).click(function(e){
			    		var seta = $('#seta');
			    		if(!seta.is(e.target)){
			    				$('#seta').attr('src','image/catalog/seta1.png');
			    			}
			    	});	

			    	$('#seta').click(function(){		
			    		
			    		if($('#texto').css('display') == 'block' ){
			    		 	$('#seta').attr('src','image/catalog/seta1.png');

			    		}else if($('#texto').css('display') == 'none'){
							$('#seta').attr('src','image/catalog/seta2.png');			
			    		}
						
			    	});
			    </script>	
			  </div>	
			</div>					
		</div>			
	</div>
	<?php }else{ ?>
	<div class="box_html <?php echo $heading_title; ?>">
		<?php echo $html; ?>
	</div>
<?php } ?>