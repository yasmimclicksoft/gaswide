<?php if($heading_title == 'box_cinza_fr'){ ?>

<div class="box_html <?php echo $heading_title; ?>">

	<div class="container" id="diferenciais">

		<h1 class="col-lg-12 col-md-12 col-sm-12" id="title_dif">NOSSOS DIFERENCIAIS</h1>

		<div class="col-sm-4 col-md-4 col-lg-4">
			<h1 class="dif"> <img class="icone_df" src="image/catalog/entrega.png"/> Entrega</h1>
			<br>
			<p id="para_entrega">Os melhores prazos de entrega do mercado.<br>
				Para as unidades em estoque, o material é <br>
				embarcado e no máximo até 48 horas após<br>
				o fechamento do pedido.</p>
			</div>

			<div class="col-sm-4 col-md-4 col-lg-4">
				<h1 class="dif"><img id="icone_sifrao" src="image/catalog/preco.png"/> Preço</h1>
				<br>
				<p id="para_preco">Somos a única loja online especializada em<br>
					cilindros para gases do mercado nacional.<br>
					Grandes contratos de compra com nossos<br>
					fabricantes nos permitem oferecer o melhor<br>
					preço aos nossos clientes. Compare!</p>
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					<h1 class="dif"><img class="icone_df" src="image/catalog/atendimento.png"/> Atendimento</h1>
					<br>
					<p id="para_atendimento">Uma equipe comercial técnica e dinâmica a<br>
						sua disposição, sempre apta a indicar o<br>
						produto certo para sua necessidade, dentro<br>
						das melhores condições de mercado.</p>
					</div>

				</div>

			</div>

			<?php } elseif ($heading_title == 'box_verde_fr') { ?>

			<div id="estatico" class="box_html <?php echo $heading_title; ?>">
									
				<div id="box-gradiente" class="row">
				<div class="col-sm-2">
					<br><br><br>
					<img id="cil" src="image/catalog/cilindro.png"/>
				</div>
				<div class="col-sm-3">
					<p id="p1">
						A PRIMEIRA</br>LOJA ONLINE ESPECIALIZADA EM CILINDROS PARA GASES DO AR
					</p>
				</div>
				<div class="col-sm-7">
					<p id="p2">
						Seja bem-vindo! Aqui você vai encontrar uma grande variedade em cilindros para gases para aplicações comuns do seu dia a dia, de forma simples, prática e dinâmica.
						</br></br>
						Os produtos dos mais renomados fabricantes do mundo reunidos em um só lugar, sempre com grandes estoques para pronta entrega, os melhores preços do mercado e a segurança do atendimento às normas de todas as autoridades regulatórias brasileiras e internacionais do setor.
						</br></br>
					</p>	
					<p id="p3">PODE COMPARAR, NÃO HÁ MELHOR PREÇO NA INTERNET!</p>
					
					<div id="unidades"  class="col-sm-11">
						 <i id="seta" class="fa fa-chevron-circle-right"></i>
					     
					     <span id="deseja">DESEJA MAIS DE <b>9 UNIDADES?</b></span>
					     
						 <p id="texto" style="opacity:0;">
						      Preços muito diferenciados para quantidades acima de 09 unidades.
						      Consulte-nos para compras no atacado! Ligue (21) 2260 8724.
						 </p>
					 </div>
					
					 <script>
					  	$("#seta").click(function(){

					  		if($('#texto').css('display') == 'none') {
							  		
							  		
						  			$("#texto").toggle();
						  			$("#texto").css('opacity','1');
							  		$("#seta").removeClass("fa-chevron-circle-right");
							  		$("#seta").addClass("fa-chevron-circle-down");
							  		("#texto").css('display','block');
							  				
						  	}
						  	else {
						  		
							  	$("#texto").toggle();
							  	$("#seta").removeClass("fa-chevron-circle-down");
					    		$("#seta").addClass("fa-chevron-circle-right");
					    		$("#texto").css('opacity','0');
					    		  			
						  	}

					  	}); 
   					</script>
				  </div>

		    </div>		

					

			</div>	
		
		<!--<img id="banner_estatico" class="img-responsive" src="catalog/view/theme/theme539/image/estatico-hd.png"/>
		$("#seta").click(function(){

					  		if($('#texto').css('display') == 'none') {
							  		
							  		$("#unidades").css('position','absolute');	
						  			$("#texto").toggle();
							  		$("#seta").removeClass("fa-chevron-circle-right");
							  		$("#seta").addClass("fa-chevron-circle-down");
							  		$("#texto").css('display','block');	
							  				
						  	}
						  	else {
						  		
							  	$("#texto").toggle();
							  	$("#seta").removeClass("fa-chevron-circle-down");
					    		$("#seta").addClass("fa-chevron-circle-right");
					    		$("#unidades").css('position','relative');
					    		  			
						  	}

					  	}); 
					-->


			</div>

			<?php }else{ ?>

			<div class="box_html <?php echo $heading_title; ?>">
				<?php echo $html; ?>
			</div>

			<?php } ?>