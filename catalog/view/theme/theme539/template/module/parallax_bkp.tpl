<div id="parallax_<?php echo $module; ?>" class="parallax" >

	<?php foreach ($banners as $banner) { ?>
	<div <?php if ($banner['link']) { ?>class="link" onclick="location.href='<?php echo $banner['link']; ?>'"<?php } ?>  <?php if ($banner['image']) { ?>data-source-url="<?php echo $banner['image']; ?>"<?php } ?> class="<?php echo $banner['title']; ?> <?php if ($banner['link']) { ?>link<?php } ?>">
		<div class="container">
			<?php // echo $banner['description']; ?>

			<?php if($module == '0'){ ?>

			<div id="mini-box-parallax1" class="parallax_super_fr col-sm-8 col-md-6 col-lg-6">

				<h1><b>Scubas LUXFER</b></h1>
				<p>Os cilindros para mergulho mais conceituados do mundo com o melhor preço do Brasil. A Gaswide mantém milhares de unidades em estoque para o mercado amador e profissional.
				</p>

			</div>

			<?php } elseif ($module == '1'){ ?>

			<div id="mini-box-parallax2" class="parallax_super_fr2 col-sm-8 col-md-6 col-lg-6">

				<h1><b>Distribuidor exclusivo LUXFER</b></h1>
				<p>Toda a excelência do maior fabricante do mundo à disposição da sua saúde. Aqui você encontra diversos modelos e pronta-entrega com os melhores preços do Brasil</p>

			</div>

			<?php } ?>
		</div>
	</div>

	
	<?php } ?>

</div>

<script>
	jQuery(document).ready(function() {
		jQuery("#parallax_<?php echo $module; ?>>div").cherryFixedParallax({
			invert: false,
			bgfixed: false
		});    
	}); 
</script>
