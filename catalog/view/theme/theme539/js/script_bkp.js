var gl_path = '';
function include(scriptUrl) {
    if (gl_path.length == 0) {
        gl_path = jQuery('#gl_path').html()
    }
    document.write('<script src="' + gl_path + scriptUrl + '"></script>');
}

/* Easing library
 ========================================================*/
include('js/jquery.easing.1.3.js');

/* ToTop
 ========================================================*/
;
(function ($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        include('js/jquery.ui.totop.js');

        $(document).ready(function () {
            $().UItoTop({easingType: 'easeOutQuart'});
        });
    }
})(jQuery);


/* SMOOTH SCROLLING
 ========================================================*/
;
(function ($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        include('js/jquery.mousewheel.min.js');
        include('js/jquery.simplr.smoothscroll.min.js');
        $(document).ready(function () {
            $.srSmoothscroll({
                step: 120,
                speed: 600
            });
        });
    }
})(jQuery);

/* Unveil
========================================================*/
;
(function ($) {
    var o = $('.lazy img');

    if (o.length > 0) {
        include('js/jquery.unveil.js');

        $(document).ready(function () {
            $(o).unveil(0, function () {
                $(this).load(function () {
                    $(this).parent().addClass("lazy-loaded");
                })
            });
        });

        $(window).load(function () {
            $(window).trigger('lookup.unveil');
            if ($('body').hasClass('common-home')) {
                $('.product-carousel .cloned .lazy').each(function () {
                    $(this).addClass('lazy-loaded');
                    var img = $(this).find('img');
                    var path = img.data('src');
                    img.attr('src', path);
                });
            }
        });

    }
})(jQuery);

/* Elevate zoom
 ========================================================*/
;
(function ($) {
    var o = $('#gallery_zoom');
    if (o.length > 0) {
        include('js/elavatezoom/jquery.elevatezoom.js');
        $(document).ready(function () {
            o.bind("click", function (e) {
                var ez = o.data('elevateZoom');
                $.fancybox(ez.getGalleryList());
                return false;
            });

            o.elevateZoom({
                gallery: 'image-additional',
                cursor: 'pointer',
                zoomType: 'inner',
                galleryActiveClass: 'active',
                imageCrossfade: true
            });
        });
    }
})(jQuery);

/* Bx Slider
 ========================================================*/
;
(function ($) {
    var o = $('#image-additional');
    var o2 = $('#gallery');
    if (o.length || o2.length) {
        include('js/jquery.bxslider/jquery.bxslider.js');
    }

    if (o.length) {
        $(document).ready(function () {
            $('#image-additional').bxSlider({
                mode: 'vertical',
                pager: false,
                controls: true,
                slideMargin: 13,
                minSlides: 5,
                maxSlides: 5,
                slideWidth: 88,
                nextText: '<i class="fa fa-chevron-down"></i>',
                prevText: '<i class="fa fa-chevron-up"></i>',
                infiniteLoop: false,
                adaptiveHeight: true,
                moveSlides: 1
            });
        });
    }

    if (o2.length) {
        include('js/photo-swipe/klass.min.js');
        include('js/photo-swipe/code.photoswipe.jquery-3.0.5.js');
        include('js/photo-swipe/code.photoswipe-3.0.5.min.js');
        $(document).ready(function () {
            $('#gallery').bxSlider({
                pager: false,
                controls: true,
                minSlides: 1,
                maxSlides: 1,
                infiniteLoop: false,
                moveSlides: 1
            });
        });
    }

})(jQuery);

/* FancyBox
 ========================================================*/
;
(function ($) {
    var o = $('.quickview');
    var o2 = $('#default_gallery');
    if (o.length > 0 || o2.length > 0) {
        include('js/fancybox/jquery.fancybox.pack.js');
    }

    if (o.length) {
        $(document).ready(function () {
            o.fancybox({
                maxWidth: 800,
                maxHeight: 600,
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'elastic',
                closeEffect: 'elastic'
            });
        });
    }

})(jQuery);

/* Superfish menus
 ========================================================*/
;
(function ($) {
    include('js/superfish.js');
    $(window).load(function () {
        $('#tm_menu .menu').superfish();
    });
})(jQuery);


/* Google Map
 ========================================================*/
;
(function ($) {
    var o = document.getElementById("google-map");
    if (o) {
        document.write('<script src="//maps.google.com/maps/api/js?sensor=false"></script>');
        include('js/jquery.rd-google-map.js');

        $(document).ready(function () {
            var o = $('#google-map');
            if (o.length > 0) {
                o.googleMap({
                    marker: {
                        basic: gl_path + 'image/gmap_marker.png',
                        active: gl_path + 'image/gmap_marker.png'
                    },
                    styles: [
                        {
                            "featureType": "administrative",
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "color": "#444444"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "saturation": -100
                                },
                                {
                                    "lightness": 45
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "color": "#46bcec"
                                },
                                {
                                    "visibility": "on"
                                }
                            ]
                        }
                    ]
                });
            }
        });
    }
})
(jQuery);

/* Owl Carousel
 ========================================================*/
;
(function ($) {
    var o = $('.related-slider');
    var o2 = $('.common-home .product-carousel');
    if (o.length || o2.length) {
        include('js/owl.carousel.min.js');
    }
    if (o.length > 0) {
        $(document).ready(function () {
            o.owlCarousel({
                smartSpeed: 450,
                dots: false,
                nav: true,
                loop: true,
                margin: 30,
                navClass: ['owl-prev fa fa-angle-left', 'owl-next fa fa-angle-right'],
                responsive: {
                    0: {items: 1},
                    500: {items: 2},
                    992: {items: 3},
                    1199: {items: 4}
                }
            });
        });
    }
    if (o2.length) {
        $(document).ready(function () {
            o2.owlCarousel({
                smartSpeed: 450,
                dots: false,
                nav: true,
                loop: true,
                margin: 30,
                navClass: ['owl-prev fa fa-angle-left', 'owl-next fa fa-angle-right'],
                responsive: {
                    0: {items: 1},
                    500: {items: 2},
                    992: {items: 3},
                    1199: {items: 4}
                }
            });
        });
    }
})(jQuery);

/* GREEN SOCKS
 ========================================================*/
;
(function ($) {
    var o = $('html');
    if (o.hasClass('desktop') && o.find('body').hasClass('common-home')) {
        include('js/greensock/jquery.gsap.min.js');
        include('js/greensock/TimelineMax.min.js');
        include('js/greensock/TweenMax.min.js');
        include('js/greensock/jquery.scrollmagic.min.js');
    }
})(jQuery);

/* Swipe Menu
 ========================================================*/
;
(function ($) {
    $(document).ready(function () {
        $('#page').click(function () {
            if ($(this).parents('body').hasClass('ind')) {
                $(this).parents('body').removeClass('ind');
                return false
            }
        })

        $('.swipe-control').click(function () {
            if ($(this).parents('body').hasClass('ind')) {
                $(this).parents('body').removeClass('ind');
                $(this).removeClass('active');
                return false
            }
            else {
                $(this).parents('body').addClass('ind');
                $(this).addClass('active');
                return false
            }
        })
    });

})(jQuery);

/* EqualHeights
 ========================================================*/
;
(function ($) {
    var o = $('[data-equal-group]');
    if (o.length > 0) {
        include('js/jquery.equalheights.js');
    }
})(jQuery);

$(document).ready(function () {
    /***********CATEGORY DROP DOWN****************/
    $("#menu-icon").on("click", function () {
        $("#menu-gadget .menu").slideToggle();
        $(this).toggleClass("active");
    });

    $('#menu-gadget .menu').find('li>ul').before('<i class="fa fa-angle-down"></i>');
    $('#menu-gadget .menu li i').on("click", function () {
        if ($(this).hasClass('fa-angle-up')) {
            $(this).removeClass('fa-angle-up').parent('li').find('> ul').slideToggle();
        }
        else {
            $(this).addClass('fa-angle-up').parent('li').find('> ul').slideToggle();
        }
    });
    if ($('.breadcrumb').length) {
        var o = $('.breadcrumb li:last-child');
        var str = o.find('a').html();
        o.find('a').css('display', 'none');
        o.append('<span>' + str + '</span>');
    }

 
    $('.menu-wr').hover(function (e) {
        e.preventDefault();
        var menu = $('.nav__primary');
        if (menu.hasClass('active')) {
            menu.fadeOut(300);
            $('.zoomContainer').css('display', 'block');
        } else {
            menu.fadeIn(300);
            $('.zoomContainer').css('display', 'none');
        }
        $(this).toggleClass('active');
        menu.toggleClass('active');
    });
    

});

var flag = true;

function respResize() {
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    if ($('aside').length) {
        var leftColumn = $('aside');
    } else {
        return false;
    }


    if (width > 767) {
        if (!flag) {
            flag = true;
            leftColumn.insertBefore('#content');
            $('.col-sm-3 .box-heading').unbind("click");

            $('.col-sm-3 .box-content').each(function () {
                if ($(this).is(":hidden")) {
                    $(this).slideToggle();
                }
            })
        }
    } else {
        if (flag) {
            flag = false;
            leftColumn.insertAfter('#content');

            $('.col-sm-3 .box-content').each(function () {
                if (!$(this).is(":hidden")) {
                    $(this).parent().find('.box-heading').addClass('active');
                }
            });

            $('.col-sm-3 .box-heading').bind("click", function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active').parent().find('.box-content').slideToggle();
                }
                else {
                    $(this).addClass('active');
                    $(this).parent().find('.box-content').slideToggle();
                }
            })
        }
    }
}

$(window).resize(function () {
    clearTimeout(this.id);
    this.id = setTimeout(respResize, 500);
});

var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

/***********************************/
if (!isMobile) {
    /***********************Green Sock*******************************/

    $(document).ready(function () {
        var stickMenu = false;
        var docWidth = $('body').find('.container').width();
        // init controller
        //controller = new ScrollMagic();
    })


    function listBlocksAnimate(block, element, row, offset, difEffect) {
        if ($(block).length) {
            var i = 0;
            var j = row;
            var k = 1;
            var effect = -1;

            $(element).each(function () {
                i++;

                if (i > j) {
                    j += row;
                    k = i;
                    effect = effect * (-1);
                }

                if (effect == -1 && difEffect == true) {
                    ef = TweenMax.from(element + ':nth-child(' + i + ')', 0.5, {
                        left: -1 * 200 - i * 300 + "px",
                        alpha: 0,
                        ease: Power1.easeOut
                    })

                } else {
                    ef = TweenMax.from(element + ':nth-child(' + i + ')', 0.5, {
                        right: -1 * 200 - i * 300 + "px",
                        alpha: 0,
                        ease: Power1.easeOut
                    })
                }

                var scene_new = new ScrollScene({
                    triggerElement: element + ':nth-child(' + k + ')',
                    offset: offset,
                }).setTween(ef)
                    .addTo(controller)
                    .reverse(false);
            });
        }
    }

    function listTabsAnimate(element) {
        if ($(element).length) {
            TweenMax.staggerFromTo(element, 0.3, {alpha: 0, rotationY: -90, ease: Power1.easeOut}, {
                alpha: 1,
                rotationY: 0,
                ease: Power1.easeOut
            }, 0.1);
        }
    }

    $(window).load(function () {
        //if ($(".fluid_container").length) {
        //    var welcome = new TimelineMax();
        //
        //    welcome.from(".fluid_container h2", 0.5, {top: -300, autoAlpha: 0})
        //        .from(".fluid_container h4", 0.5, {bottom: -300, autoAlpha: 0});
        //
        //    var scene_welcome = new ScrollScene({
        //        triggerElement: ".fluid_container",
        //        offset: -100
        //    }).setTween(welcome).addTo(controller).reverse(false);
        //}
    });
}

/* Troca de Cores na categoria na página de categorias */


$(function () {

 var categoria = $('#titulo_categoria').text();
        
        switch(categoria){

            case 'Oxigenoterapia':
                $('#botao_cart').css('background-color','#3ab849');
                $('#grade').css('color','#3ab849');
                $('.fa-th-list').css('color','#3ab849');
                $('.rating .fa-star + .fa-star-o').css('color','#3ab849');
                $('.fa-star').css('color','#3ab849');
                $('.menu-toggle').css('background-color','#3ab849');
                $('.sale').css('background-color','#3ab849');
                $('.btn').css('background-color','#3ab849');
                $('.product-btn-add').css('background-color','#3ab849');
                $('#compare-total').css('background-color','#3ab849');
                $('#label_categoria').attr('src','image/catalog/oxi.png');
            break;
            
            case 'Industrial':
                $('#botao_cart').css('background-color','#424242');
                $('#grade').css('color','#424242');
                $('.fa-th-list').css('color','#424242');
                $('.rating .fa-star + .fa-star-o').css('color','#424242');
                $('.fa-star').css('color','#424242');
                $('.menu-toggle').css('background-color','#424242');
                $('.sale').css('background-color','#424242');
                $('.btn').css('background-color','#424242');
                $('.product-btn-add').css('background-color','#424242');
                $('#compare-total').css('background-color','#424242');
                $('#label_categoria').attr('src','image/catalog/industrial.png');                
            break;
  
            case "Hélio/ Fly Balloon":
                $('#botao_cart').css('background-color','#e89223');
                $('#grade').css('color','#e89223');
                $('.fa-th-list').css('color','#e89223');
                $('.rating .fa-star + .fa-star-o').css('color','#e89223');
                $('.fa-star').css('color','#e89223');
                $('.menu-toggle').css('background-color','#e89223');
                $('.sale').css('background-color','#e89223');
                $('.btn').css('background-color','#e89223');
                $('.product-btn-add').css('background-color','#e89223');
                $('#compare-total').css('background-color','#e89223');
                $('#label_categoria').attr('src','image/catalog/helio.png');
            break;
            
            case "Chopp/ Gás Carbônico":
                $('#botao_cart').css('background-color','#697b91');
                $('#grade').css('color','#697b91');
                $('.fa-th-list').css('color','#697b91');
                $('.rating .fa-star + .fa-star-o').css('color','#697b91');
                $('.fa-star').css('color','#697b91');
                $('.menu-toggle').css('background-color','#697b91');
                $('.sale').css('background-color','#697b91');
                $('.btn').css('background-color','#697b91');
                $('.product-btn-add').css('background-color','#697b91');
                $('#compare-total').css('background-color','#697b91');
                $('#label_categoria').attr('src','image/catalog/chopp.png');
            break;

            case "Mergulho/ Ar Respirável":
                $('#botao_cart').css('background-color','#d7da00');
                $('#grade').css('color','#d7da00');
                $('.fa-th-list').css('color','#d7da00');
                $('.rating .fa-star + .fa-star-o').css('color','#d7da00');
                $('.fa-star').css('color','#d7da00');
                $('.menu-toggle').css('background-color','#d7da00');
                $('.sale').css('background-color','#d7da00');
                $('.btn').css('background-color','#d7da00');
                $('.product-btn-add').css('background-color','#d7da00');
                $('#compare-total').css('background-color','#d7da00');
                $('#label_categoria').attr('src','image/catalog/mergulho.png');
            break;

            case "Misturas de Calibração":
                $('#botao_cart').css('background-color','#008fc8');
                $('#grade').css('color','#d7da00');
                $('.fa-th-list').css('color','#d7da00');
                $('.rating .fa-star + .fa-star-o').css('color','#d7da00');
                $('.fa-star').css('color','#d7da00');
                $('.menu-toggle').css('background-color','#008fc8');
                $('.sale').css('background-color','#008fc8');
                $('.btn').css('background-color','#008fc8');
                $('.product-btn-add').css('background-color','#008fc8');
                $('#compare-total').css('background-color','#008fc8');
                $('#label_categoria').attr('src','image/catalog/misturas.png');
            break;                
            default :
                $('#botao_cart').css('background-color','#377035');
                $('#grade').css('color','#377035');
                $('#grade').css('color','#377035');
                $('.rating .fa-star + .fa-star-o').css('color','#377035');
                $('.fa-star').css('color','#377035');
                $('.menu-toggle').css('background-color','#377035');
                $('.sale').css('background-color','#377035');
                $('.btn').css('background-color','#377035');
                $('.product-btn-add').css('background-color','#377035');
                $('#compare-total').css('background-color','#377035');
                $('#label_categoria').attr('src','image/catalog/misturas.png');        
            break;
        }
    

    });          