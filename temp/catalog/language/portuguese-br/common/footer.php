<?php
// Text
$_['text_information']  = 'Informações';
$_['text_service']      = 'Serviços ao cliente';
$_['text_extra']        = 'Outros serviços';
$_['text_contact']      = 'Contato';
$_['text_return']       = 'Devoluções';
$_['text_sitemap']      = 'Mapa do site';
$_['text_manufacturer'] = 'Produtos por marca';
$_['text_voucher']      = 'Comprar vale presentes';
$_['text_affiliate']    = 'Programa de afiliados';
$_['text_special']      = 'Produtos em promoção';
$_['text_account']      = 'Minha conta';
$_['text_order']        = 'Histórico de compras';
$_['text_wishlist']     = 'Lista de desejos';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Desenvolvido por ClickSoft';