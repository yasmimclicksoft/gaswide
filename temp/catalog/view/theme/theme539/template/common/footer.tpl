<footer>
    <div class="bg-shadow">
        <div class="container">

            <div class="row">

                <div class="box_html form_cormecial_fr">

                        <div class="info_contato_fr col-sm-4 col-md-4 col-lg-4">

                            <p class="texto_cinza_fr cinza_1_fr" style="font-size:26px">Fale agora com a nossa</p>
                            <p class="texto_branco_fr branco_1_fr" style="font-size:29px;">EQUIPE COMERCIAL</p>
                            
                            <p id="email-comercial" class="texto_cinza_fr comercial ">comercial@gaswide.com</p>        
                            
                            <div  class="row">
                             <div class="telefone_comercial_fr">
                                         <span style="padding-right: 0px;
    padding-top: 20px;" class="col-md-2">
                                         <img id="icon-tel" src="image/catalog/tel.png">
                                         </span>
                                       
                                        <p>
                                         <p class="telefone_mini_fr">    
                                        (21)&nbsp</p><p class="texto_branco_fr branco_1_fr num">2260 8724</p>
                                        <p>
                                        <p>
                                        <p class="telefone_mini_fr">(21)&nbsp</p><p class="texto_branco_fr branco_1_fr num">2270 3549</p>
                                        </p>
                                        </div>  
                                </div>
                            </div>
                            <!--<p class="texto_branco_fr">Rua Felizardo Fortes, 400 - Ramos, Rio de Janeiro - RJ</p> -->

                            <form id="contato" method="post" action="contato_comercial.php">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <input type="text" id="empresa_comercial_fr" class="input_form_com_fr rodape" name="empresa" placeholder="Empresa">
                                <br>
                                <input type="text" class="input_form_com_fr rodape" name="nome" placeholder="Nome">
                                <br>
                                <input type="text" class="input_form_com_fr rodape" name="email" placeholder="Email">
                                <br>
                                <input type="text" id="telefone_comercial_fr" class="input_form_com_fr rodape" name="telefone" placeholder="Telefone com DDD">

                            </div>

                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <textarea name="mensagem" cols='40' rows='6' placeholder="Envie sua dúvida ou solicite uma cotação aqui."></textarea>
                                <br>
                                <input type="submit" id="btn_cont_comercial_fr" value="ENVIAR MENSAGEM">
                            </div>
                        </form>
                        

                    </div>

                </div>

                <!--div class="col-sm-12">
                    <div class="footer-line"></div>
                </div-->
                <div class="col-sm-12" id="horario"><p>Horário de funcionamento: Segunda a Sexta 8:30h às 17:30h</p></div>
                
                <div id="options_rodape" class="row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <?php if ($informations) { ?>
                    <div class="footer_box">
                        <h5 data-equal-group="3"><?php echo $text_information; ?></h5>
                        <ul class="list-unstyled">
                            <?php foreach ($informations as $information) { ?>
                            <li>
                                <a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
                            </li>
                            <?php } ?>
                            <li>
                                <a target="_blank" title="Termos e condições" href="pdf/termosecondicoes.pdf">Termos e condições</a>
                            </li>
                            <li>
                                <a target="_blank" title="Regras de segurança" href="pdf/regrasdeseguranca.pdf">Cilindros: Regras de segurança</a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_box serv">
                        <h5 data-equal-group="3" ><?php echo $text_service; ?></h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $return; ?>"><?php echo $text_return; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
               
                <div class="col-lg-3 col-md-3 col-sm-3" >
                    <div class="footer_box">
                        <h5 data-equal-group="3"><?php echo $text_account; ?></h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $order; ?>"><?php echo $text_order; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a>
                            </li>
                            <li>
                                <a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a>
                            </li>

                        </ul>
                    </div>
                </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
   
        <div class="row copyright">

         <div class="container ">

            <div class="col-md-4 images-footer despachamos">
                <img src="image/catalog/despachamos.png">
            </div>

            <div class="col-md-4 images-footer">
                <img src="image/catalog/pronta-entrega.png">
            </div>

            <div class="col-md-4 images-footer">
                <img src="image/catalog/pagamento.png">
            </div>
            <div class="col-md-12" id="link-click">
                 <a href="http://clicksoft.com.br/" target="blank"><?php echo $powered; ?></a> 
            </div>
        </div>

     
    </div>
    <!--div class="copyright">
        <div class="container">
         
            <div class="row" id="despache_row">
            <div class="despache_footer_fr m_footer_cop_fr col-sm-4 col-md-4 col-lg-4">
                <div class="col-sm-2 despache">
                  <img class="icone_df" src="image/catalog/cart-cinza.png"/>
                </div>
                <div id="desp" class="col-sm-2 despache">
                    <p id="p_d">DESPACHAMOS PARA</p>
                    <h1 >TODO O BRASIL</h1>
                </div>
            </div>

            <div class="pronta_entrega_fr m_footer_cop_fr col-sm-4 col-md-4 col-lg-4">
                <div>
                    <h2>Pronta Entrega</h2>
                    <p>Faturamos com a aprovação de cadastro</p>
                </div>
            </div>

            <div class="imagem_pagamento_fr m_footer_cop_fr col-sm-4 col-md-4 col-lg-4">
                <img src="image/catalog/bandeiras.png">
            </div>
            </div>
             <div class="row" id="clickrow">
                 <a href="http://clicksoft.com.br/" target="blank"><?php echo $powered; ?></a>
            </div>
        </div-->

</footer>
<script src="<?php echo $global_path ?>js/livesearch.js" type="text/javascript"></script>
<script src="<?php echo $global_path ?>js/script.js" type="text/javascript"></script>
</div>
<script src="<?php echo $global_path ?>js/jquery.maskedinput.min.js"></script>

<script>
    jQuery(function($){
      
       $("#input-telephone").mask("?(99) 99999-9999"); 
    });
    
    jQuery(function($){ 
       $("#input-payment-telephone").mask("?(99) 99999-9999");
    });
     jQuery(function($){ 
       $("#telefone_comercial_fr").mask("?(99) 99999-9999");
    });
    

</script>
<!--&lt;!&ndash; begin olark code &ndash;&gt;-->
<!--<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){-->
<!--f[z]=function(){-->
<!--(a.s=a.s||[]).push(arguments)};var a=f[z]._={-->
<!--},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){-->
<!--f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={-->
<!--0:+new Date};a.P=function(u){-->
<!--a.p[u]=new Date-a.p[0]};function s(){-->
<!--a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){-->
<!--hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){-->
<!--return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){-->
<!--b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{-->
<!--b.contentWindow[g].open()}catch(w){-->
<!--c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{-->
<!--var t=b.contentWindow[g];t.write(p());t.close()}catch(x){-->
<!--b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({-->
<!--loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});-->
<!--/* custom configuration goes here (www.olark.com/documentation) */-->
<!--olark.identify('4417-797-10-1102');/*]]>*/</script><noscript><a href="https://www.olark.com/site/4417-797-10-1102/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>-->
<!--&lt;!&ndash; end olark code &ndash;&gt;-->
</body></html>